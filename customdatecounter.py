import customdate
import datehelper


def getAllWeekdaysBetween(startDate, endDate, weekday):
    """Return a list of all the dates in ascending order of the given weekdays between specified dates

    Keyword arguments:
    startDate -- CustomDate object from which to start searching (included in search)
    endDate   -- CustomDate object where to end searching (included in search)
    weekday   -- Integer where 0 is monday and 6 is sunday (same as datetime.weekday())
    """
    dates = []
    if startDate.weekday == weekday:
        dates.append(startDate)
    # could also use startDate, but this is clearer to me
    currentDate = getNextWeekday(startDate, weekday)
    while not endDate < currentDate:
        dates.append(currentDate)
        currentDate = getNextWeekday(currentDate, weekday)
    return dates


def getNextWeekday(currentDate, weekday):
    """Returns CustomDate object of the next occurence of specified datetime.

    Keyword arguments:
    date    -- CustomDate object representing current day
    weekday -- Integer where 0 is monday and 6 is sunday (same as datetime.weekday())
    """
    daysToGo = weekday - currentDate.weekday if currentDate.weekday < weekday else 7 - currentDate.weekday + weekday
    day = currentDate.day + daysToGo
    month = currentDate.month
    year = currentDate.year

    # if we go over the days of the month we have to increase month
    if(day > datehelper.daysInMonth(month, year)):
        day = day % datehelper.daysInMonth(month, year)
        # if we are not on december just increase the month
        if(month < 12):
            month += 1
        # else we also need to change year and start from january
        else:
            month = 1
            year += 1

    # advance date the amount of days needed and return it
    return customdate.CustomDate(day, month, year)


def dateToString(date):
    # lets have zero-padded string representation here too
    return "{:02d}.{:02d}.{:04d}".format(date.day, date.month, date.year)


def stringToDate(string):
    # this just splits the string from '.' and then gives it as arguments
    # doesn't have any error handling so the string should be days.months.years
    return customdate.CustomDate(*[int(date) for date in string.strip().split('.')])
