import unittest
import counter
import datehelper
import customdate
import customdatecounter as cd


class TestCounter(unittest.TestCase):
    def test_get_next_weekday(self):
        # if given a sunday and asked for sunday it should return the next sunday
        self.assertEqual(counter.stringToDate("2.4.2017"), counter.getNextWeekday(counter.stringToDate("26.3.2017"), 6))
        # if given a saturday and asked for sunday it should give the next day
        self.assertEqual(counter.stringToDate("26.3.2017"), counter.getNextWeekday(counter.stringToDate("25.3.2017"), 6))
        # if given wednesday and asked for monday it should give the next weeks monday
        self.assertEqual("03.04.2017", counter.dateToString(counter.getNextWeekday(counter.stringToDate("29.3.2017"), 0)))

    def test_given_example(self):
        given = ["03.02.2013", "10.02.2013", "17.02.2013", "24.02.2013", "03.03.2013"]
        startDate = counter.stringToDate("2.2.2013")
        endDate = counter.stringToDate("3.3.2013")

        dates = counter.getAllWeekdaysBetween(startDate, endDate, 6)
        for i, date in enumerate(dates):
            self.assertEqual(counter.dateToString(date), given[i])


class TestDatehelper(unittest.TestCase):
    def test_is_leap_year(self):
        # Took a premade list from https://kalender-365.de/leap-years.php to compare against. Contains years 1904-2400)
        leapYears = [1904,1908,1912,1916,1920,1924,1928,1932,1936,1940,1944,1948,1952,1956,1960,1964,1968,1972,1976,1980,1984,1988,1992,1996,2000,2004,2008,2012,2016,2020,2024,2028,2032,2036,2040,2044,2048,2052,2056,2060,2064,2068,2072,2076,2080,2084,2088,2092,2096,2104,2108,2112,2116,2120,2124,2128,2132,2136,2140,2144,2148,2152,2156,2160,2164,2168,2172,2176,2180,2184,2188,2192,2196,2204,2208,2212,2216,2220,2224,2228,2232,2236,2240,2244,2248,2252,2256,2260,2264,2268,2272,2276,2280,2284,2288,2292,2296,2304,2308,2312,2316,2320,2324,2328,2332,2336,2340,2344,2348,2352,2356,2360,2364,2368,2372,2376,2380,2384,2388,2392,2396,2400]
        count = 0
        for i in range(1900, 2401):
            if datehelper.isLeapYear(i):
                self.assertTrue(i in leapYears)
                count += 1
        self.assertEqual(len(leapYears), count)

    def test_dates_in_month(self):
        # Test normal year february
        self.assertEqual(28, datehelper.daysInMonth(2, 2017))
        # Test leap year february
        self.assertEqual(29, datehelper.daysInMonth(2, 2016))


class TestCustomDate(unittest.TestCase):
    def test_weekdays_right(self):
        # check correct monday
        self.assertEqual(0, customdate.CustomDate(20, 3, 2017).weekday)
        # check correct monday on leapday
        self.assertEqual(0, customdate.CustomDate(29, 2, 2016).weekday)
        # check correct thursday
        self.assertEqual(3, customdate.CustomDate(23, 3, 2017).weekday)

        # check fullweek as correct (1.8.2016 was monday)
        for i in range(7):
            self.assertEqual(i, customdate.CustomDate(i+1, 8, 2016).weekday)


class TestCustomDateCounter(unittest.TestCase):
    def test_string_to_date(self):
        date = cd.stringToDate("1.1.1900")
        self.assertEqual(1, date.day)
        self.assertEqual(1, date.month)
        self.assertEqual(1900, date.year)
        self.assertEqual(0, date.weekday)
        date2 = cd.stringToDate("31.12.2500")
        self.assertEqual(31, date2.day)
        self.assertEqual(12, date2.month)
        self.assertEqual(2500, date2.year)
        self.assertEqual(4, date2.weekday)

    def test_given_example(self):
        given = ["03.02.2013", "10.02.2013", "17.02.2013", "24.02.2013", "03.03.2013"]
        startDate = cd.stringToDate("2.2.2013")
        endDate = cd.stringToDate("3.3.2013")

        dates = cd.getAllWeekdaysBetween(startDate, endDate, 6)
        for i, date in enumerate(dates):
            self.assertEqual(cd.dateToString(date), given[i])

    def test_against_datetime_class(self):
        startDate = "1.1.1900"
        endDate = "1.1.2500"

        datesWithout = cd.getAllWeekdaysBetween(cd.stringToDate(startDate), cd.stringToDate(endDate), 6)
        dates = counter.getAllWeekdaysBetween(counter.stringToDate(startDate), counter.stringToDate(endDate), 6)

        for i, date in enumerate(datesWithout):
            self.assertEqual(cd.dateToString(date), counter.dateToString(dates[i]))


if __name__ == '__main__':
    unittest.main()
