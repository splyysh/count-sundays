from datetime import timedelta, datetime

DEFAULT_FORMAT = "%d.%m.%Y"


def getNextWeekday(currentDate, weekday):
    """Returns datetime.date object of the next occurence of specified datetime.

    Keyword arguments:
    date    -- Python datetime.date object from which to start searching.
    weekday -- Integer where 0 is monday and 6 is sunday (same as datetime.weekday())
    """
    daysToGo = weekday - currentDate.weekday() if currentDate.weekday() < weekday else 7 - currentDate.weekday() + weekday
    # advance date the amount of days needed and return it
    return currentDate + timedelta(days=daysToGo)


def getAllWeekdaysBetween(startDate, endDate, weekday):
    """Return a list of all the dates in ascending order of the given weekdays between specified dates

    Keyword arguments:
    startDate -- datetime.date object from which to start searching (included in search)
    endDate   -- datetime.date object where to end searching (included in search)
    weekday   -- Integer where 0 is monday and 6 is sunday (same as datetime.weekday())
    """
    dates = []
    if startDate.weekday() == weekday:
        dates.append(startDate)
    currentDate = getNextWeekday(startDate, weekday)
    while currentDate <= endDate:
        dates.append(currentDate)
        currentDate = getNextWeekday(currentDate, weekday)
    return dates


def dateToString(date):
    # easiest way to change the formatting to look as wanted (in otherwords remove zero-padding from dates)
    # return date.strftime(DEFAULT_FORMAT).lstrip("0").replace(".0", ".")
    # instead we are not actually gonna bother since zero-padded dates just look better and I don't think
    # the assignment is strict on this
    return date.strftime(DEFAULT_FORMAT)


def stringToDate(string):
    return datetime.strptime(string, DEFAULT_FORMAT)
