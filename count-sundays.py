from enum import Enum
import argparse
import sys

# You can change which one you use here, they should produce the same output
from customdatecounter import *
# from counter import *


def printDates(dates, file=sys.stdout):
    """prints a list of dates.

        By default this prints the dates to stdout, but a file to write can be specified
        in file argument.

        Keyword arguments:
        dates  -- a list of date objects to be printed. The list should be in the order you want it printed
        file   -- file where the function should print the dates to. By default stdout.

    """
    for date in dates:
        print(dateToString(date), file=file)


def printToScreen(startDate, endDate, dates):
    """Prints list of dates to stdout as specified in the assignment"""
    print("\nSundays between dates {} and {} were:".format(dateToString(startDate), dateToString(endDate)))
    printDates(dates, sys.stdout)


def printToFile(startDate, endDate, dates, filename="CountingSundays.txt"):
    """Prints list of dates to a file named filename as specified in the assignment"""
    with open(filename, "w") as f:
        print("Starting date {}:".format(dateToString(startDate)), file=f)
        printDates(dates, f)
        print("Ending date {}".format(dateToString(endDate)), file=f)


def getDate(question):
    """Asks user for a date and return datetime.object"""
    correctInput = False
    while not correctInput:
        dateStr = input(question)
        try:
            date = stringToDate(dateStr)
            return date
        except ValueError:
            print("Date given in wrong format\n")


def getInput(question, possibleAnswers):
    answer = input(question)
    while answer not in possibleAnswers:
        print("Please input one of the possible answers")
        answer = input(question)
    return answer


def uiHandler():
    """Handles user input after program is run"""
    startDate = getDate("Give me the first date:")
    endDate = getDate("Give me the second date:")

    # Get all of the sundays between start and end (6 = sunday)
    dates = getAllWeekdaysBetween(startDate, endDate, 6)

    destination = getInput("Would you like to get information on (s)creen, (f)ile or (b)oth?", ["s", "f", "b"])
    order = getInput("Would you like to get this information in (a)scending or (d)escending order?", ["a", "d"])

    if order == "d":
        dates.reverse()

    # Print to screen if user chooses so
    if destination == "s" or destination == "b":
        printToScreen(startDate, endDate, dates)

    # Print to file if user chooses so. We never ask for filename so we will just use the default
    if destination == "f" or destination == "b":
        printToFile(startDate, endDate, dates)


def terminalHandler(options):
    startDate = stringToDate(options.startDate)
    endDate = stringToDate(options.endDate)
    dates = getAllWeekdaysBetween(startDate, endDate, 6)
    if options.descending:
        dates.reverse()
    printToFile(startDate, endDate, dates, options.filename)
    if(options.printToScreen):
        printToScreen(startDate, endDate, dates)


if __name__ == "__main__":
    # lets add argumentparser to enable running the program from terminal without any ui
    parser = argparse.ArgumentParser(description="""Tells you all of the dates between start date and end date that are of certain weekday.
You have to specify atleast start date and end date if you want to use this without ui, otherwise interactive ui will be ran""")
    parser.add_argument("-s", "--start",
                        action="store", type=str, dest="startDate", default=None,
                        help="Sets the start date. Should be in dd.mm.yyyy")
    parser.add_argument("-e", "--end",
                        action="store", type=str, dest="endDate", default=None,
                        help="Sets the end date. Should be in dd.mm.yyyy")
    parser.add_argument("-o", "--output",
                        action="store", type=str, dest="filename",  default="CountingSundays.txt",
                        help="Sets the output filename")
    parser.add_argument("-v", "--verbose",
                        action="store_true", dest="printToScreen", default=False,
                        help="Prints the output to the screen too")
    parser.add_argument("-d", "--descending",
                        action="store_true", dest="descending", default=False,
                        help="Prints the dates in descending order (default is ascending)")
    options = parser.parse_args()

    if(options.startDate is not None and options.endDate is not None):
        terminalHandler(options)
    else:
        uiHandler()
