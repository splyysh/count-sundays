"""
Some helper functions to deal with dateobjects. Not usually very useful since you shouldn't implement dates on your own
"""


def isLeapYear(year):
    """Returns true if the given year is a leap year"""
    return (year % 400 == 0) or (year % 4 == 0 and year % 100 != 0)


def daysInMonth(month, year):
    """Returns how many days there are in a given month on a given year"""
    month = month - 1
    normalDaysPerMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    if(month == 1 and isLeapYear(year)):
        return 29
    else:
        return normalDaysPerMonth[month]


def daysFrom1900(day, month, year):
    """Calculates the number of dates from 1.1.1900 (monday) to given day"""
    days = 0
    for m in range(1, month):
        days += daysInMonth(m, year)
    for y in range(1900, year):
        days += 366 if isLeapYear(y) else 365
    return days + day - 1
