# The assignment:
To create a simple program to count sundays between to dates given as string. I chose to do the assignment in python, both with `datetime` library and with my own minimal `customdate` library that has the bare minimum to achieve the task. I do it both ways because from the assigment it seems it wants me to create my own, but in actual scenarious I wouldn't touch date functions without libraries if not absolutely necessary since it is so error prone and full of edge-cases and exceptions. As a plus the `datetime` version will let me test my own solution against probably more correct version.

## Usage
Now my solution can be run in interactive mode like in the assigment below by just running `python count-sundays.py` and following instructions.
```zsh
(p35) ➜  count-sundays git:(master) ✗ python count-sundays.py
Give me the first date:2.2.2013
Give me the second date:3.3.2013
Would you like to get information on (s)creen, (f)ile or (b)oth?s
Would you like to get this information in (a)scending or (d)escending order?d

Sundays between dates 02.02.2013 and 03.03.2013 were:
03.03.2013
24.02.2013
17.02.2013
10.02.2013
03.02.2013
```
Other option is to run directly from the terminal by providing the correct parameters. Some examples below.

```zsh
(p35) ➜  count-sundays git:(master) ✗ python count-sundays.py --help
usage: count-sundays.py [-h] [-s STARTDATE] [-e ENDDATE] [-o FILENAME] [-v]
                        [-d]

Tells you all of the dates between start date and end date that are of certain
weekday. You have to specify atleast start date and end date if you want to
use this without ui, otherwise interactive ui will be ran

optional arguments:
  -h, --help            show this help message and exit
  -s STARTDATE, --start STARTDATE
                        Sets the start date. Should be in dd.mm.yyyy
  -e ENDDATE, --end ENDDATE
                        Sets the end date. Should be in dd.mm.yyyy
  -o FILENAME, --output FILENAME
                        Sets the output filename
  -v, --verbose         Prints the output to the screen too
  -d, --descending      Prints the dates in descending order (default is
                        ascending)
```
```zsh
(p35) ➜  count-sundays git:(master) ✗ python count-sundays.py -s 2.2.2013 -e 3.3.2013 -o count.txt -v

Sundays between dates 02.02.2013 and 03.03.2013 were:
03.02.2013
10.02.2013
17.02.2013
24.02.2013
03.03.2013
```
```zsh
(p35) ➜  count-sundays git:(master) ✗ python count-sundays.py -s 2.2.2013 -e 3.3.2013 -o count.txt -v -d 

Sundays between dates 02.02.2013 and 03.03.2013 were:
03.03.2013
24.02.2013
17.02.2013
10.02.2013
03.02.2013
```

```zsh
(p35) ➜  count-sundays git:(master) ✗ cat count.txt
Starting date 02.02.2013:
03.03.2013
24.02.2013
17.02.2013
10.02.2013
03.02.2013
Ending date 03.03.2013
```

## Python Programming Exam:
### Theory:
You are given the following information, but you may prefer to do some research for yourself.

* 1 Jan 1900 was a Monday.

* Thirty days has September, April, June and November.

* All the rest have thirty­one,

* Saving February alone,

* Which has twenty­eight, rain or shine.

* And on leap years, twenty­nine.

* A leap year occurs on any year evenly divisible by 4, but not on a century unless it is divisible by 400

### TASK:

The task is to create program that takes two dates as an input from user via user interface. The programs task is to count amount how many Sundays fell between these dates. 

The program should ask user the option shall it print the information to screen, file or both. Program should also be able to output information in ascending and descending format.

Program should be able start counting from 1 Jan 1900 and reach out to 1.1.2500

File name, UI look and feel can be freely chosen, but below is an example for simple (*read minimal required implementation.*)

#### Example UI:

```zsh
python count_sundays.py

Give me the first date: 2.2.2013

Give me the second date: 3.3.2013

Would you like to get information on screen(s), in file(f) or both(b)?b

Do you want this information in ascending(a) or descending(d) order:a

Sundays between dates 2.2.2013 and 3.3.2013 were:

3.2.2013

10.2.2013

17.2.2013

24.2.2013

3.3.2013
```


#### Example output file content:
```text
Counting Sundays

Starting date 2.2.2013:

3.2.2013

10.2.2013

17.2.2013

24.2.2013

3.3.2013

Ending date: 3.3.2013
```