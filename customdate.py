import datehelper


class CustomDate:
    """Custom class to help find days between date ranges. Isn't very complete for other uses.

    Expects the dates to be correct, takes day, month and year as they appear. Weekday is integer between
    0-6 where 0 is monday and 6 is sunday. This class shouldn't be used anywhere else, rather use datetime.
    """
    def __init__(self, day, month, year):
        self.day = day
        self.month = month
        self.year = year
        self.weekday = self.calculateWeekday()

    def calculateWeekday(self):
        """Internal function used in calculating which weekday is today"""
        return datehelper.daysFrom1900(self.day, self.month, self.year) % 7

    # These functions are used to check if date is smaller than another.
    # Note that you cannot compare date > date eventhough I could have coded
    # it probably faster than writing this comment (just using this as help)
    # but I didn't as I don't need it
    def __lt__(self, other):
        if self.year < other.year:
            return True
        if self.year > other.year:
            return False
        if self.month < other.month:
            return True
        if self.month > other.month:
            return False
        if self.day < other.day:
            return True
        return False

    def __eq__(self, other):
        return self.day == other.day and self.month == other.month and self.year == other.year
